package ru.volnenko.rest;

import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.TaskRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/task")
public final class TaskService {

    @Autowired
    private TaskRepository taskRepository;
    
    @GetMapping("/list")
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @PostMapping
    public Task save(@RequestBody Task task) {
    	return taskRepository.save(task);
    }
    
    @GetMapping("/{id}")
    public Task get(@PathVariable String id) {
    	return taskRepository.findById(id).orElse(null);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable String id) {
    	taskRepository.deleteById(id);
    }
}
