package ru.volnenko.boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import ru.volnenko.client.RestClient;
import ru.volnenko.se.config.JpaConfig;

@SpringBootApplication
@ComponentScan("ru.volnenko.rest")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] {Application.class, JpaConfig.class}, args);
	}
	
	@Bean
	public CommandLineRunner restClient(ApplicationContext ctx) {
	    return (args) -> {
	    	RestClient.main(args);
	    };
   }
}
